//
//  BIDViewController.h
//  Button Fun
//
//  Created by Jim Ridge on 16/11/12.
//  Copyright (c) 2012 Jim Ridge. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BIDViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *statusText;

- (IBAction)buttonPressed:(UIButton *)sender;

@end
