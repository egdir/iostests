//
//  BIDAppDelegate.h
//  one
//
//  Created by Jim Ridge on 16/11/12.
//  Copyright (c) 2012 Jim Ridge. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BIDViewController;

@interface BIDAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) BIDViewController *viewController;

@end
