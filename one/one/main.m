//
//  main.m
//  one
//
//  Created by Jim Ridge on 16/11/12.
//  Copyright (c) 2012 Jim Ridge. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BIDAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([BIDAppDelegate class]));
    }
}
